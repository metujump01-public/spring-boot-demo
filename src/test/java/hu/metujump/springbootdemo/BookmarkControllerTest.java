package hu.metujump.springbootdemo;

import hu.metujump.springbootdemo.repository.BookmarkRepository;
import hu.metujump.springbootdemo.repository.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.annotation.DeleteMapping;

import static hu.metujump.springbootdemo.MessageCode.NOT_FOUND;
import static hu.metujump.springbootdemo.MessageCode.SUCCESSFULLY_MODIFIED;
import static hu.metujump.springbootdemo.TestHelper.codeInBodyIs;
import static hu.metujump.springbootdemo.TestHelper.messageInBodyIs;
import static hu.metujump.springbootdemo.TestHelper.validJson;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookmarkController.class)
class BookmarkControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookmarkRepository bookmarkRepository;

    @Test
    @DisplayName("GET /bookmark/{id} should return with bookmarkRepository.findById(id) as a JSON")
    public void findById() throws Exception {

        Bookmark bookmark = new Bookmark();
        bookmark.setId(1234L);
        bookmark.setColor(Color.RED);
        bookmark.setDescription("Valami");
        bookmark.setUrl("http://szuper.link/alma");


        given(this.bookmarkRepository.findById(1234L))
                .willReturn(bookmark);

        this.mvc.perform(
                get("/bookmark/{id}", 1234L) // REQUEST
                        .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())            // EXPECTATIONS ABOUT THE RESPONSE
                .andExpect(header().string("content-type", startsWith("application/json")))
                .andExpect(content().json(validJson("{\n" +
                        "  \"id\": 1234,\n" +
                        "  \"description\": \"Valami\",\n" +
                        "  \"url\": \"http://szuper.link/alma\",\n" +
                        "  \"color\": \"RED\"\n" +
                        "}")));
    }

    @Nested
    @DisplayName("GET /bookmark/{id} when repository.findById() throws an EntityNotFoundException")
    class FindByIdNotFound{

        ResultActions sendRequest;
        EntityNotFoundException entityNotFoundException;
        String entityNotFoundMessage;

        @BeforeEach
        void beforeEach() throws Exception {
            reset(bookmarkRepository);

            entityNotFoundMessage = "Bookmark not found!";

            entityNotFoundException = mock(EntityNotFoundException.class);

            given(entityNotFoundException.getMessage())
                    .willReturn(entityNotFoundMessage);

            given(bookmarkRepository.findById(anyLong()))
                    .willThrow(entityNotFoundException);

            sendRequest = mvc.perform(
                    get("/bookmark/{id}", 1234L) // REQUEST
                            .accept(MediaType.APPLICATION_JSON));

        }

        @Test
        @DisplayName("should return 404 Not Found")
        public void statusNotFound() throws Exception {
            sendRequest.andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("should return a json object in the body with code='NOT_FOUND'")
        public void codeInBodyNotFound() throws Exception {
            sendRequest.andExpect(codeInBodyIs(NOT_FOUND));
        }

        @Test
        @DisplayName("should return a json object in the body with a message field " +
                "with a value of exception.getMessage()")
        public void messageInBodyFromException() throws Exception {
            sendRequest.andExpect(messageInBodyIs(entityNotFoundMessage));
            verify(entityNotFoundException).getMessage();
        }

    }



    @Test
    @DisplayName("PUT /bookmark/{id} should call bookmarkRepoitory.update() method " +
            "with the Bookmark passed as a json in the request body")
    public void update() throws Exception {

        Bookmark expectedBookmark = new Bookmark();
        expectedBookmark.setUrl("http://szuper.link/alma");
        expectedBookmark.setDescription("Valami2");
        expectedBookmark.setColor(Color.BLUE);
        expectedBookmark.setId(4567L);

        this.mvc.perform(
                put("/bookmark/{id}", 4567L) // REQUEST
                        .contentType("application/json")
                        .content(validJson("{\n" +
                                "  \"description\": \"Valami2\",\n" +
                                "  \"url\": \"http://szuper.link/alma\",\n" +
                                "  \"color\": \"BLUE\"\n" +
                                "}")))

                .andExpect(status().isOk())          // EXPECTATIONS ABOUT THE RESPONSE
                .andExpect(codeInBodyIs(SUCCESSFULLY_MODIFIED));

        verify(bookmarkRepository).update(expectedBookmark);
    }

    @Nested
    @DisplayName("POST /bookmark")
    class Create {

        Bookmark expectedBookmark;
        ResultActions sendRequest;
        Long createdId;

        @BeforeEach
        void beforeEach() throws Exception {

            reset(bookmarkRepository);

            createdId = 3456L;

            expectedBookmark = new Bookmark();
            expectedBookmark.setUrl("http://szuper.link/alma");
            expectedBookmark.setDescription("Valami2");
            expectedBookmark.setColor(Color.BLUE);

            given(bookmarkRepository.create(any()))
                    .willReturn(createdId);


            sendRequest = mvc.perform(
                    post("/bookmark") // REQUEST
                            .contentType("application/json")
                            .content(validJson("{\n" +
                                    "  \"id\": \"1234\",\n" +
                                    "  \"description\": \"Valami2\",\n" +
                                    "  \"url\": \"http://szuper.link/alma\",\n" +
                                    "  \"color\": \"BLUE\"\n" +
                                    "}")));

        }

        @Test
        @DisplayName("should call bookmarkRepository.create() method " +
                "with the Bookmark passed as a json in the request body")
        public void shouldCallRepositoryCreate() throws Exception {
            verify(bookmarkRepository).create(expectedBookmark);
        }

        @Test
        @DisplayName("should return with HTTP Status 201 Created")
        public void shouldReturnWithStatus201Created() throws Exception {
            sendRequest.andExpect(status().isCreated());
        }
        @Test
        @DisplayName("should return with a Location header pointing to /bookmark/{id}, " +
                "where id should be the one returned by repository.create()")
        public void shouldReturnWithAProperLocationHeader() throws Exception {
            sendRequest.andExpect(header().string("location","/bookmark/" + createdId));
        }

    }

    @Test
    @DisplayName("DELETE /bookmark/{id} should call repository.delete(id)")
    void deleteShouldCallRepositoryDelete() throws Exception {
        mvc.perform(
                delete("/bookmark/{id}", 1234L) // REQUEST
                        .accept(MediaType.APPLICATION_JSON));

        verify(bookmarkRepository).delete(1234L);
    }


}
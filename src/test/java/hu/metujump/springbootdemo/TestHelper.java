package hu.metujump.springbootdemo;

import org.json.JSONException;
import org.opentest4j.AssertionFailedError;
import org.skyscreamer.jsonassert.JSONParser;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class TestHelper {


    public static ResultMatcher codeInBodyIs(MessageCode messageCode) {
        return jsonPath("$.kod")
                .value(messageCode.toString());
    }

    public static ResultMatcher messageInBodyIs(String message) {
        return jsonPath("$.uzenet")
                .value(message);
    }


    public static String validJson(String json) {
        try {
            JSONParser.parseJSON(json);
            return json;
        } catch (JSONException e) {
            throw new AssertionFailedError("This is not a valid JSON string\n" + e.getMessage(), e);
        }
    }
}

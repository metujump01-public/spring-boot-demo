package hu.metujump.springbootdemo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;


import static hu.metujump.springbootdemo.MessageCode.SUCCESSFULLY_MODIFIED;
import static hu.metujump.springbootdemo.TestHelper.codeInBodyIs;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(HelloController.class)
class HelloControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GreetingProperties greetingProperties;

    @Test
    @DisplayName("GET /message/default-name should return greetingProperties.getDefaultName() as the body")
    public void getDefaultName() throws Exception {

        given(this.greetingProperties.getDefaultName())
                .willReturn("some name");

        this.mvc.perform(
                get("/message/default-name") // REQUEST
                        .accept(MediaType.TEXT_PLAIN))

                .andExpect(status().isOk())            // EXPECTATIONS ABOUT THE RESPONSE
                .andExpect(content().string("some name"));
    }



    @Test
    @DisplayName("PUT /message/default-name should call greetingProperties.setDefaultName(body content)")
    public void putDefaultName() throws Exception {

        this.mvc.perform(
                put("/message/default-name") // REQUEST
                        .contentType("text/plain")
                        .content("valami ertek"))

                .andExpect(status().isOk())          // EXPECTATIONS ABOUT THE RESPONSE
                .andExpect(codeInBodyIs(SUCCESSFULLY_MODIFIED));

        verify(greetingProperties).setDefaultName("valami ertek");
//                .andExpect(content().json("some name"));
    }

}
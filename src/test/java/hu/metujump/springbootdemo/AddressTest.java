package hu.metujump.springbootdemo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @Test
    public void testBuilder() {
        final Address address =
                Address.builder()
                        .budapest()
                        .postalCode("1234")
                        .build();


        assertEquals("Budapest", address.getCity());
        assertEquals("1234", address.getPostalCode());
    }

}
package hu.metujump.springbootdemo.service.calculator;

public class MathExpressionBuilder {

    private MathExpression currentExpression;

    private MathExpressionBuilder(long number) {
        currentExpression = new Constant(number);
    }

    public static MathExpressionBuilder startWith(long number) {
        return new MathExpressionBuilder(number);
    }

    public MathExpressionBuilder add(long number) {
        currentExpression = new Add(currentExpression, new Constant(number));
        return this;
    }

    public MathExpressionBuilder subtract(long number) {
        currentExpression = new Subtract(currentExpression, new Constant(number));
        return this;
    }

    public MathExpressionBuilder multiplyBy(long number) {
        currentExpression = new Multiply(currentExpression, new Constant(number));
        return this;
    }

    public MathExpressionBuilder divideBy(long number) {
        currentExpression = new Divide(currentExpression, new Constant(number));
        return this;
    }

    public MathExpression build() {
        return currentExpression;
    }
}

package hu.metujump.springbootdemo.service.calculator;

public class Multiply extends OperatorExpression {

    public Multiply(MathExpression leftExpression, MathExpression rightExpression) {
        super(leftExpression, rightExpression);
    }

    @Override
    protected long evaluateOperator(long left, long right) {
        return left * right;
    }

    @Override
    protected String operatorSign() {
        return "*";
    }

    @Override
    protected int getPrecedence() {
        return PRECEDENCE;
    }

    public static final int PRECEDENCE = 2;

}

package hu.metujump.springbootdemo.service.calculator;

public class Constant implements MathExpression {

    private final long value;

    public Constant(long value) {
        this.value = value;
    }

    @Override
    public long evaluate() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public String render(int precedenceOfParent) {
        return toString();
    }
}

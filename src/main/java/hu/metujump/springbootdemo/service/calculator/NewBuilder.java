package hu.metujump.springbootdemo.service.calculator;

public class NewBuilder {

    private MathExpression currentExpression;
    private OperatorToCreate operatorToCreate = null;

    private NewBuilder(long number) {

        currentExpression = new Constant(number);
    }

    public static NewBuilder startWith(long number) {
        return new NewBuilder(number);
    }

    void newOperator(OperatorConstructor constructor, int precedence, MathExpression valueExpression) {
        if (operatorToCreate != null && operatorToCreate.precedence >= precedence) {
            currentExpression = operatorToCreate.build();
            operatorToCreate = null;
        }
        operatorToCreate = new OperatorToCreate(constructor, precedence, valueExpression, operatorToCreate);
    }

    public NewBuilder add(long number) {
        newOperator(Add::new, Add.PRECEDENCE, new Constant(number));
        return this;
    }

    public NewBuilder subtract(long number) {
        newOperator(Subtract::new, Subtract.PRECEDENCE, new Constant(number));
        return this;
    }

    public NewBuilder multiplyBy(long number) {
        newOperator(Multiply::new, Multiply.PRECEDENCE, new Constant(number));
        return this;
    }

    public NewBuilder divideBy(long number) {
        newOperator(Divide::new, Divide.PRECEDENCE, new Constant(number));
        return this;
    }

    class OperatorToCreate {
        private final OperatorConstructor constructor; // the constructor Function Interface of the OperatorExpression
        private final int precedence;            // the precedence of the current operator
        private MathExpression right;            // the right operand of the operator, usually a Constant
        private final OperatorToCreate previous; // reference of the previous instance (can be null)

        public OperatorToCreate(OperatorConstructor constructor, int precedence, MathExpression right, OperatorToCreate previous) {
            this.constructor = constructor;
            this.precedence = precedence;
            this.right = right;
            this.previous = previous;
        }

        public MathExpression build() {
            MathExpression left;
            // Do we have a previous OperatorToCreate object?
            if (previous != null) {
                left = previous.right; // the left operand is previous.right

                final MathExpression operatorExpression
                        = constructor.construct(left, right); // construct the OperatorExpression

                previous.right = operatorExpression; // replace previous.right

                return previous.build(); // nesting: do the same algorithm with the previous ObjectToCreate object
            } else {
                left = currentExpression; // the left operand is the currentExpression
                return constructor.construct(left, right); // construct the OperationExpression and return with it
            }
        }
    }

    interface OperatorConstructor {
        MathExpression construct(MathExpression left, MathExpression right);
    }

    public MathExpression build() {
        if (operatorToCreate != null) {
            currentExpression = operatorToCreate.build();
            operatorToCreate = null;
        }
        return currentExpression;
    }
}

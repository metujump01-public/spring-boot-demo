package hu.metujump.springbootdemo.service.calculator;

public interface MathExpression {

    long evaluate();

    /**
     *
     * @return
     */
    String toString();

    String render(int precedenceOfParent);
}

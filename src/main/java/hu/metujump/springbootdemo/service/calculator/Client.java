package hu.metujump.springbootdemo.service.calculator;

public class Client {
    public static void main(String[] args) {

        final MathExpression expression = new Multiply(
                new Add(
                        new Constant(123),
                        new Add(
                                new Constant(456),
                                new Constant(789))),
                new Subtract(
                        new Constant(3),
                        new Constant(4)
                ));

        //System.out.println(expression + " = " + expression.evaluate());

        final MathExpression expression1 = MathExpressionBuilder
                .startWith(1)
                .add(2)
                .add(3)
                .multiplyBy(4)
                .subtract(5)
                .divideBy(5).build();

        final MathExpression expression2 = NewBuilder
                .startWith(1)
                .add(2)
                .add(3)
                .multiplyBy(4)
                .subtract(5)
                .divideBy(5).build();


        System.out.println(expression1 + " = " + expression1.evaluate());
        System.out.println(expression2 + " = " + expression2.evaluate());

    }
}

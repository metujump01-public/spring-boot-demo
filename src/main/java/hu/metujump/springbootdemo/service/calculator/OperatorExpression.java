package hu.metujump.springbootdemo.service.calculator;

public abstract class OperatorExpression implements MathExpression {

    protected MathExpression leftExpression;
    protected MathExpression rightExpression;

    public OperatorExpression(MathExpression leftExpression, MathExpression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public long evaluate() {
        long left = leftExpression.evaluate();
        long right = rightExpression.evaluate();
        return evaluateOperator(left, right);
    }

    protected abstract long evaluateOperator(long left, long right);

    @Override
    public String toString() {
        return render(Integer.MIN_VALUE);
    }

    protected abstract String operatorSign();

    @Override
    public String render(int precedenceOfParent) {

        final int myPrecedence = getPrecedence();

        String leftRendered = leftExpression.render(myPrecedence);
        String rightRendered = rightExpression.render(myPrecedence);

        String rendered = leftRendered + " " + operatorSign() + " " + rightRendered;

        if (precedenceOfParent > myPrecedence) {
            rendered = "(" + rendered + ")";
        }

        return rendered;
    }

    protected abstract int getPrecedence();
}

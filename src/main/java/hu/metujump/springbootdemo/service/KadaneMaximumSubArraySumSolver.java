package hu.metujump.springbootdemo.service;

import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KadaneMaximumSubArraySumSolver implements MaximumSubArraySumSolver {

    static class Kadane implements IntBinaryOperator {

        private int maxEndsHere = 0;

        @Override
        public int applyAsInt(int maxEver, int current) {
            maxEndsHere = Math.max(current, maxEndsHere + current);
            return Math.max(maxEver, maxEndsHere);
        }
    }


    @Override
    public int solve(int[] array) {
        return IntStream.of(array)
                .sequential()
                .reduce(new Kadane())
                .orElse(0);
    }

    public static void main(String[] args) {
        final KadaneMaximumSubArraySumSolver solver = new KadaneMaximumSubArraySumSolver();
        final int result = solver.solve(new int[]{-12,-13,-8,-9});

        System.out.println(result);
    }
}

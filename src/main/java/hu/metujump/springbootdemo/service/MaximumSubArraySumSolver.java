package hu.metujump.springbootdemo.service;

public interface MaximumSubArraySumSolver {
    int solve(int[] array);
}

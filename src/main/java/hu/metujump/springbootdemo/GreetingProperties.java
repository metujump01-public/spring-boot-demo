package hu.metujump.springbootdemo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.util.Date;

@ConfigurationProperties("greeting")
public class GreetingProperties {
    private String template;
    private String defaultName;

    @NestedConfigurationProperty
    private Address address;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String formatGreeting(String name) {
        return String.format(template, name != null ? name : defaultName, new Date());
    }
}

package hu.metujump.springbootdemo;

public class Customer {
    private Address address;
    private String firstName;
    private String lastName;

    public Customer() {
    }

    public Customer(Customer other) {
        this.address = new Address(other.address);
        this.firstName = other.firstName;
        this.lastName = other.lastName;
    }

    public Customer(Address address, String firstName, String lastName) {
        this.address = address;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "address=" + address +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public static void main(String[] args) {
        final Address a1 = Address.builder().city("Nagykanizsa").build();
        final Customer bela = new Customer(a1, "Bela", "Kovacs");
        final Customer belaCopy = new Customer(bela);
        //belaCopy.setFirstName("Feri");
        System.out.println("bela:     " + bela);
        System.out.println("belaCopy: " + belaCopy);
        belaCopy.setFirstName("Feri");
        belaCopy.getAddress().setStreet("Jozsef Attila utca");
        System.out.println("bela:     " + bela);
        System.out.println("belaCopy: " + belaCopy);
    }
}

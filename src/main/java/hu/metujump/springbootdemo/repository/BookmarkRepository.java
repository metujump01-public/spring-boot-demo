package hu.metujump.springbootdemo.repository;


import hu.metujump.springbootdemo.Bookmark;

import java.util.List;

public interface BookmarkRepository {

    /**
     *
     * @param bookmark
     * @return
     * @throws UniqueConstraintViolatedException
     * @throws NullPointerException when bookmark is null
     * or bookmark.getUrl() is null or bookmark.getDescription() is null.
     */
    long create(Bookmark bookmark) throws UniqueConstraintViolatedException, NullPointerException;

    /**
     * Returns with a bookmark identified by the given id.
     * @param id
     * @return the found Bookmark object with the given id.
     * @throws EntityNotFoundException thrown when the Bookmark could not be found with the specified id.
     */
    Bookmark findById(long id) throws EntityNotFoundException;
    List<Bookmark>findAll();
    void update(Bookmark bookmark) throws EntityNotFoundException, UniqueConstraintViolatedException;
    void delete(long id) throws EntityNotFoundException;

}

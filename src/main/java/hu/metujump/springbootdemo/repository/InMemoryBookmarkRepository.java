package hu.metujump.springbootdemo.repository;

import hu.metujump.springbootdemo.Bookmark;
import hu.metujump.springbootdemo.Color;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class InMemoryBookmarkRepository implements BookmarkRepository {

    private Map<Long,Bookmark>bookmarkById = new HashMap<>();
    private AtomicLong counter = new AtomicLong();

    private long generateId() {
        return counter.incrementAndGet();
    }

    private void throwEntityNotFoundExceptionIfDoesNotExist(Long id) {
        if(!bookmarkById.containsKey(id)) {
            //dobjunk EntityNotFoundException-t, ha nincs.
            throw new EntityNotFoundException("Nem található ilyen azonosítóval bookmark!");
        }
    }

    @Override
    public long create(Bookmark bookmark) throws NullPointerException, UniqueConstraintViolatedException {
        //ellenőzrés
        Objects.requireNonNull(bookmark, "A bookmark paraméter kötelező");
        Objects.requireNonNull(bookmark.getUrl(), "Az url nem lehet null!");
        Objects.requireNonNull(bookmark.getDescription(), "A description nem lehet null!");
        //geneneráljunk egy ID-t
        long id = generateId();

        final Bookmark managedBookmark = new Bookmark(bookmark);

        //állítsuk be az id-t a bookmarkban
        managedBookmark.setId(id);
        //tegyük bele a hashmapbe
        bookmarkById.put(id, managedBookmark);

        return id;
    }

    @Override
    public Bookmark findById(long id) throws EntityNotFoundException {
        //ellenőrízzük, hogy van-e ilyen id-val létező bookmark.
        throwEntityNotFoundExceptionIfDoesNotExist(id);

        //keressük meg
        final Bookmark managedBookmark = bookmarkById.get(id);

        //másoljuk le a managed változatot
        final Bookmark copy = new Bookmark(managedBookmark);
        return copy;
    }

    @Override
    public List<Bookmark> findAll() {
//        for(Map.Entry<Long,Bookmark> entry: bookmarkById.entrySet()) {
//            entry.
//        }

//        Function<Bookmark, Bookmark> copyBookmark =
//                new Function<Bookmark, Bookmark>() {
//                    @Override
//                    public Bookmark apply(Bookmark managedBookmark) {
//                        return new Bookmark(managedBookmark);
//                    }
//                };
//
//        copyBookmark = managedBookmark -> new Bookmark(managedBookmark);
//
//        copyBookmark = Bookmark::new;

        return bookmarkById.values()
                .stream()
                .map(Bookmark::new)
                .collect(Collectors.toList());

//        List<Bookmark>result = new ArrayList<>();
//
//        final Collection<Bookmark> managedBookmarks = bookmarkById.values();
//
//
//        for(Bookmark managedBookmark: managedBookmarks) {
//            Bookmark copy = new Bookmark(managedBookmark);
//            result.add(copy);
//        }
//
////        return new ArrayList<>(bookmarkById.values());
//        return result;
    }

    @Override
    public void update(Bookmark bookmark) throws EntityNotFoundException, UniqueConstraintViolatedException {
        //ellenőrzés
        Objects.requireNonNull(bookmark, "A bookmark paraméter kötelező");
        final Long id = bookmark.getId();
        Objects.requireNonNull(id, "Az id nem lehet null!");
        Objects.requireNonNull(bookmark.getUrl(), "Az url nem lehet null!");
        Objects.requireNonNull(bookmark.getDescription(), "A description nem lehet null!");
        //megkeressük id alapján a már létező, menedzselt Bookmark objektumot

        //ellenőrízzük, hogy van-e ilyen id-val létező bookmark.
        throwEntityNotFoundExceptionIfDoesNotExist(id);

        //keressük meg
        final Bookmark managedBookmark = bookmarkById.get(id);

        managedBookmark.setUrl(bookmark.getUrl());
        managedBookmark.setDescription(bookmark.getDescription());
        managedBookmark.setColor(bookmark.getColor());

    }


    @Override
    public void delete(long id) throws EntityNotFoundException {

//        Object idAsLong = id; // autoboxing happens here
//        System.out.println(idAsLong.getClass().getName());
        throwEntityNotFoundExceptionIfDoesNotExist(id);
        bookmarkById.remove(id);
    }

    public static void main(String[] args) {
        final InMemoryBookmarkRepository repo = new InMemoryBookmarkRepository();
        Bookmark b1 = new Bookmark();
        b1.setUrl("http://index.hu");
        b1.setDescription("Index");
        final long id = repo.create(b1);
        final long id2 = repo.create(b1);
 //       b1.setId(123L);

        final Bookmark fromRepo = repo.findById(id);
        System.out.println(fromRepo);
        fromRepo.setId(123L);
        fromRepo.setDescription("Hehehehe");

        final Bookmark fromRepo2 = repo.findById(id);
        System.out.println(fromRepo2);

        final List<Bookmark> all = repo.findAll();
        System.out.println(all);
        all.get(0).setId(1234L);

        final List<Bookmark> all2 = repo.findAll();
        System.out.println(all2);

        Bookmark b1u = new Bookmark();
        b1u.setUrl("http://origo.hu");
        b1u.setDescription("Origo.hu");
        b1u.setColor(Color.RED);
        b1u.setId(id2);

        repo.update(b1u);

        System.out.println(repo.findAll());

        repo.delete(id);

        System.out.println(repo.findAll());

    }
}

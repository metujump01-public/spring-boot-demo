package hu.metujump.springbootdemo;

public enum MessageCode {
    SUCCESSFULLY_CREATED,
    SUCCESSFULLY_MODIFIED,
    NOT_FOUND
}

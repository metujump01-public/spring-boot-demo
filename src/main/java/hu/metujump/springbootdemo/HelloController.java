package hu.metujump.springbootdemo;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloController {

    private GreetingProperties greetingProperties;

    public HelloController(GreetingProperties greetingProperties) {
        this.greetingProperties = greetingProperties;
    }

    @GetMapping("/")
    public String hello() {
        return greetingProperties.formatGreeting(null);
    }

    @GetMapping("/greet/{name}")
    public String greet(@PathVariable String name) {
        return greetingProperties.formatGreeting(name);
    }

    @GetMapping("/city")
    public String getCity() {
        return greetingProperties.getAddress().getCity();
    }

    @GetMapping("/message/default-name")
    public String getDefaultName() {
        return greetingProperties.getDefaultName();
    }

    @PutMapping(path = "/message/default-name", consumes = "text/plain",
            produces = {"application/json","application/xml"})
    public Message setDefaultName(@RequestBody String defaultName) {

        greetingProperties.setDefaultName(defaultName);

            return new Message(MessageCode.SUCCESSFULLY_MODIFIED,
                    "Default-name is modified successfully");
    }
}

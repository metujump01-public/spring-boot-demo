package hu.metujump.springbootdemo;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "szerver-uzenet")
public class Message {
    private MessageCode code;
    private String message;


    public Message() {
    }

    public Message(MessageCode code, String message) {
        this.code = code;
        this.message = message;
    }

    @JsonProperty("kod")
    @XmlAttribute(name="kod")
    public MessageCode getCode() {
        return code;
    }

    @JsonProperty("uzenet")
    @XmlValue
    public String getMessage() {
        return message;
    }

    public void setCode(MessageCode code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package hu.metujump.springbootdemo;

import java.util.Objects;

public class Bookmark {
    private String url;
    private String description;
    private Color color;
    private Long id;

    public Bookmark() {
    }

    public Bookmark(Bookmark original) {
        this.id = original.id;
        this.url = original.url;
        this.color = original.color;
        this.description = original.description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bookmark bookmark = (Bookmark) o;
        return Objects.equals(url, bookmark.url) &&
                Objects.equals(description, bookmark.description) &&
                color == bookmark.color &&
                Objects.equals(id, bookmark.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(url, description, color, id);
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", color=" + color +
                ", id=" + id +
                '}';
    }
}

package hu.metujump.springbootdemo;

import hu.metujump.springbootdemo.repository.BookmarkRepository;
import hu.metujump.springbootdemo.repository.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/bookmark", produces = "application/json")
public class BookmarkController {

    private BookmarkRepository repository;

    public BookmarkController(BookmarkRepository repository) {
        this.repository = repository;
    }

    @ExceptionHandler(EntityNotFoundException.class)
    ResponseEntity<Message> handleEntityNotFoundException(EntityNotFoundException ex) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Message(MessageCode.NOT_FOUND, ex.getMessage()));
    }

    @GetMapping("{id}")
    Bookmark findById(@PathVariable long id) {
        return repository.findById(id);
    }

    @GetMapping
    List<Bookmark> findAll() {
        return repository.findAll();
    }

    @PutMapping(path = "{id}",consumes = "application/json")
    Message update(@PathVariable long id, @RequestBody Bookmark bookmark) {
        bookmark.setId(id);
        repository.update(bookmark);
        return new Message(MessageCode.SUCCESSFULLY_MODIFIED, "Siker!");
    }

    @PostMapping(consumes = "application/json")
    ResponseEntity<Message> create(@RequestBody Bookmark bookmark) {
        bookmark.setId(null);
        final long id = repository.create(bookmark);

        return ResponseEntity
                .status(201)
                .header("Location", "/bookmark/" + id)
                .body(new Message(MessageCode.SUCCESSFULLY_CREATED, "Sikeresen létrehoztuk!"));
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable long id) {
        repository.delete(id);
    }

}

package hu.metujump.springbootdemo;

public class Address {
    private String poBox;
    private String country;
    private String city;
    private String street;
    private String postalCode;

    public Address() {

    }

    public Address(String poBox, String country, String city, String street, String postalCode) {
        this.poBox = poBox;
        this.country = country;
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
    }

    public Address(Address other) {
        this.poBox = other.poBox;
        this.country = other.country;
        this.city = other.city;
        this.street = other.street;
        this.postalCode = other.postalCode;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "poBox='" + poBox + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }

    public static AddressBuilder builder() {
        return new AddressBuilder();
    }

    public static class AddressBuilder {

        private String city;
        private String postalCode;

        public AddressBuilder city(String city) {
            this.city = city;
            return this;
        }
        public AddressBuilder postalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public AddressBuilder budapest() {
            this.city = "Budapest";
            return this;
        }

        public Address build() {
            return new Address(null, null, city, null, postalCode);
        }

    }



}
